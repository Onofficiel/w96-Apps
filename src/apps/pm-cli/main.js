const { term } = env;
const { PkMgr } = w96.sys;

if (!term) return;

class PkgMgrCli extends WApplication {
  constructor() {
    super();
  }

  cmdReg = [
    {
      name: ["help"],
      exec: (...args) => this.showHelp(...args),
      desc: "Shows help",
    },
    {
      name: ["add", "install", "i"],
      exec: (...args) => this.installPackage(...args),
      args: ["<...packages>"],
      desc: "Installs a package",
    },
    {
      name: ["remove", "uninstall", "r"],
      exec: (...args) => this.removePackage(...args),
      args: ["<...packages>"],
      desc: "Removes a package",
    },
    {
      name: ["repo-add"],
      exec: (...args) => this.addRepo(...args),
      args: ["<...repos>"],
      desc: "Adds a repo to the sources",
    },
    {
      name: ["repo-remove"],
      exec: (...args) => this.removeRepo(...args),
      args: ["<...repos>"],
      desc: "Removs a repo from the sources",
    },
  ];

  async main(argv) {
    super.main(argv);

    this.title = "Package Manager CLI";

    // Command handler:
    if (!argv[1]) {
      this.showHelp();
    } else {
      let i = 0;
      for (const cmd of this.cmdReg) {
        if (cmd.name.includes(argv[1])) {
          await cmd.exec(argv.slice(2));
        } else if (++i >= this.cmdReg.length) {
          await this.showHelp(0);
        }
      }
    }
  }

  async installPackage(pkgs) {
    if (!pkgs.length) return term.println("No package name specified");

    const pm = new PkMgr();

    await pm.performChecks();
    await pm.readSources();
    await pm.reloadPackageCache();

    for (const pkgName of pkgs) {
      const pkg = pm.getPackage(pkgName);

      if (!pkg) {
        term.println(`The package ${JSON.stringify(pkgName)} was not found`);
        continue;
      }

      if (
        (await pm.getPackageInstallState(`${pkg.name}:${pkg.repoId}`)) ===
        "INSTALLED"
      ) {
        term.println(
          `The package ${JSON.stringify(pkgName)} is already installed`
        );
        continue;
      }

      await pm.installPackage(pkg, (data) => {
        term.println(`[${pkg.name}]: ${data.message}`);
      });
    }
  }

  async removePackage(pkgs) {
    if (!pkgs.length) return term.println("No package name specified");

    const pm = new PkMgr();

    await pm.performChecks();
    await pm.readSources();
    await pm.reloadPackageCache();

    for (const pkgName of pkgs) {
      const pkg = pm.getPackage(pkgName);

      if (!pkg) {
        term.println(`The package ${JSON.stringify(pkgName)} was not found`);
        continue;
      }

      if (
        (await pm.getPackageInstallState(`${pkg.name}:${pkg.repoId}`)) ===
        "NOT_INSTALLED"
      ) {
        term.println(`The package ${JSON.stringify(pkgName)} is not installed`);
        continue;
      }

      await pm.removePackage(pkg, (data) => {
        term.println(`[${pkg.name}]: ${data.message}`);
      });
    }
  }

  async addRepo(repos) {
    await new PkMgr().performChecks();

    const config = JSON.parse(
      await FS.readstr("C:/system/packages/configs/sources.json")
    );

    for (let repo of repos) {
      repo.endsWith("/") && (repo = repo.slice(0, -1));

      if (config.sources.includes(repo)) {
        term.println(
          `The repo ${JSON.stringify(repo)} is already included in sources`
        );
        continue;
      }

      config.sources.push(repo);
      term.println(
        `The repo ${JSON.stringify(repo)} is now included in sources`
      );
    }

    await FS.writestr(
      "C:/system/packages/configs/sources.json",
      JSON.stringify(config, null, 4)
    );
  }

  async removeRepo(repos) {
    await new PkMgr().performChecks();

    const config = JSON.parse(
      await FS.readstr("C:/system/packages/configs/sources.json")
    );

    for (let repo of repos) {
      repo.endsWith("/") && (repo = repo.slice(0, -1));

      if (!config.sources.includes(repo)) {
        term.println(
          `The repo ${JSON.stringify(repo)} is not included in sources`
        );
        continue;
      }

      config.sources.splice(config.sources.indexOf(repo), 1);
      term.println(`The repo ${JSON.stringify(repo)} is removed from sources`);
    }

    await FS.writestr(
      "C:/system/packages/configs/sources.json",
      JSON.stringify(config, null, 4)
    );
  }

  showHelp(showTitle = 1) {
    if (showTitle) {
      term.println(term.color.blue("Package Manager CLI"));
    }
    term.println(
      ["Usage: pm <command> [arguments]", "", "Commands:"].join("\n")
    );
    for (const cmd of this.cmdReg) {
      term.println(
        `    ${cmd.name.join("/")}${cmd.args ? ` ${cmd.args.join(" ")}` : ""}${
          cmd.desc ? ` - ${cmd.desc}` : ""
        }`
      );
    }
    term.println("");
  }
}
