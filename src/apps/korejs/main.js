// Polyfill
if (!term) {
  term = {
    onData() {},
    println(e) {
      alert(e);
    },
    printError(e) {
      alert(e, { icon: "error" });
    },
    color: {
      yellow: (e) => e,
      blue: (e) => e,
      red: (e) => e,
    },
  };
}

class KoreJS extends WApplication {
  constructor() {
    super();
  }

  cmdReg = [
    {
      name: ["run"],
      exec: (...args) => this.runFile(...args),
      args: ["[flags]", "<path>", "[args]"],
      desc: "Executes a JavaScript script from a file",
    },
    {
      name: ["version", "v"],
      exec: (...args) => this.printVersion(...args),
      args: [],
      desc: "Gives the current KoreJS version",
    },
    {
      name: ["init"],
      exec: (...args) => this.initProject(...args),
      args: ["...[path]"],
      desc: "Inits a new KoreJS project",
    },
    {
      name: ["repo"],
      exec: (...args) => this.manageRepo(...args),
      args: ["<add|remove <name=url> ...[name=url]>|<list>"],
      desc: "Manages repositories",
    },
    {
      name: ["pkg"],
      exec: (...args) => this.managePkg(...args),
      args: ["<add|remove <name> ...[name]>|<list>"],
      desc: "Manages packages",
    },
    {
      name: ["fmt"],
      exec: (...args) => this.formatFile(...args),
      args: ["<path>"],
      desc: "Formats JavaScript and JSON files",
    },
    {
      name: ["help"],
      exec: (...args) => this.showHelp(...args),
      args: ["[command]"],
      desc: "Shows help",
    },
  ];

  async main(argv) {
    super.main(argv);

    this.title = "Kore JS";

    // Command handler:
    if (!argv[1]) {
      this.showHelp();
    } else {
      let i = 0;
      for (const cmd of this.cmdReg) {
        if (cmd.name.includes(argv[1])) {
          await cmd.exec(argv.slice(2));
        } else if (++i >= this.cmdReg.length) {
          await this.showHelp(0);
        }
      }
    }
  }

  async createEnv(file, args, flags) {
    this.rt = new KoreRuntime(term);

    // Terminate key
    term.onData((e) => {
      if (e === "\x03") this.rt.kill();
    });

    // Loading in runtime
    await this.rt.load(file, args, flags);
  }

  async runFile(args) {
    if (!(await FS.isFile("C:/local/korejs/env.html"))) {
      await FS.mkdir("C:/local/korejs");
      await FS.writestr("C:/local/korejs/env.html", ENV_HTML);
    }

    const flags = [];
    const rtArgs = [];

    args.forEach((arg) => {
      if (arg.startsWith("-") && !rtArgs.length) flags.push(arg);
      else rtArgs.push(arg);
    });

    const pkgPath = resolvePath(cwd, "pkg.json");
    if (rtArgs[0] === "!" && (await FS.isFile(pkgPath))) {
      let pkg = JSON.parse(await FS.readstr(pkgPath));
      rtArgs[0] = pkg.fle;
    }
    const file = resolvePath(cwd, rtArgs[0]);
    if (!(await FS.exists(file)) || !(await FS.isFile(file))) {
      term.printError("File does not exists.");
      return this.terminate();
    }

    await this.createEnv(file, rtArgs, flags);
  }

  async initProject([path = "."]) {
    path = resolvePath(cwd, path);

    await FS.mkdir(resolvePath(path, "kore_pkg"));
    if (!(await FS.exists(resolvePath(path, "main.js"))))
      await FS.writestr(
        resolvePath(path, "main.js"),
        [
          `/* * * * * * * * * * * * * * * * * * * * * * * * *\\`,
          `* Entry file                                      *`,
          `*                                                 *`,
          `* This file is the one that will be executed      *`,
          "* when `kore run !` is ran in this directory.     *",
          `\\* * * * * * * * * * * * * * * * * * * * * * * * */`,
          ``,
          `function greet() {`,
          `  console.log("%cHello ${
            PKG_FILE_NAMES[Math.floor(Math.random() * PKG_FILE_NAMES.length)]
          }!", "color: ${
            PKG_FILE_COLORS[Math.floor(Math.random() * PKG_FILE_COLORS.length)]
          }");`,
          `}`,
          ``,
          `greet();`,
        ].join("\n")
      );

    await FS.writestr(
      resolvePath(path, "pkg.json"),
      formatJSON(
        JSON.stringify({
          pkg: await term.prompt("Package Name: "),
          frn: await term.prompt("Friendly Name: "),
          fle: "main.js",

          aut: await term.prompt("Author: "),
          dsc: await term.prompt("Description: "),

          ver: await term.prompt("Version: "),
          rpo: {},
          dep: [],
        })
      )
    );
    term.println("Initialisation done");
  }

  async formatFile(paths) {
    if (paths.length <= 0)
      paths = await FS.walk(cwd).then((p) =>
        p.aFilter(
          async (path) =>
            (await FS.isFile(path)) &&
            [".js", ".json"].includes(FSUtil.getExtension(path))
        )
      );

    paths.forEach(async (path) => {
      if (!path) throw "No file specified";

      const file = resolvePath(cwd, path);
      if (!(await FS.isFile(file))) throw "Wrong path included";

      term.println("Formatting " + file);

      const content = await FS.readstr(file);

      try {
        await FS.writestr(
          file,
          FSUtil.getExtension(file) === ".js"
            ? formatJS(content)
            : formatJSON(content)
        );
      } catch (e) {
        term.printError(`Error while formatting the file: ${e}`);
      }
    });
  }

  async printVersion() {
    term.println(`KoreJS v${VERSION.toString()}`);
  }

  async manageRepo([method, ...urls]) {
    if (method === "add") {
      if (urls.length <= 0) return term.printError("No URL was passed");
      const path = resolvePath(cwd, "pkg.json");

      for (const args of urls) {
        const splitted = args.split("=");

        let [name, url] = splitted;
        if (splitted.length !== 2)
          return term.printError("Bad argument format");

        try {
          url = new URL(url).toString();
        } catch {
          return term.printError(`Bad URL for ${name}`);
        }

        const pkgJson = JSON.parse(await FS.readstr(path));
        pkgJson.rpo[name] = url.endsWith("/") ? url : `${url}/`;
        await FS.writestr(path, formatJSON(JSON.stringify(pkgJson)));

        await term.println(`Repository ${name} added`);
      }
    } else if (method === "remove") {
      if (urls.length <= 0) return term.printError("No repository was passed");
      const path = resolvePath(cwd, "pkg.json");

      for (const repo of urls) {
        const pkgJson = JSON.parse(await FS.readstr(path));
        delete pkgJson.rpo[repo];
        await FS.writestr(path, formatJSON(JSON.stringify(pkgJson)));

        await term.println(`Repository ${repo} removed`);
      }
    } else if (method === "list") {
      const path = resolvePath(cwd, "pkg.json");

      const pkgJson = JSON.parse(await FS.readstr(path));
      await term.println(
        `Repositories in "${cwd}":\n${Object.entries(pkgJson.rpo)
          .map(([k, v]) => `  ${k}: ${v}`)
          .join("\n")}`
      );
    } else {
      term.printError("Bad method");
    }
  }

  async managePkg([method, ...pkgs]) {
    const pkgPath = resolvePath(cwd, "pkg.json");
    const pkgJson = JSON.parse(await FS.readstr(pkgPath));

    if (method === "add") {
      if (pkgs.length > 0) {
        for (const dep of pkgs) {
          // Check KPN
          let { repo, pkg, ver } = parseKPN(dep);
          if (!pkg || !repo) {
            return term.printError("Bad Kore Package Notation");
          }

          ver ||= (await getPackages(cwd, repo)).find(
            (p) => p.name === pkg
          ).version;

          const kpn = `${repo}:${pkg}@${ver}`;
          if (pkgJson.dep.includes(kpn)) {
            term.println(`${kpn} is already included`);
          } else {
            pkgJson.dep.push(kpn);
            await FS.writestr(pkgPath, formatJSON(JSON.stringify(pkgJson)));
          }
        }
      }
      await installDeps(cwd);
    } else if (method === "remove") {
      for (const dep of pkgs) {
        let { repo, pkg } = parseKPN(dep);
        if (!pkg || !repo) {
          return term.printError("Bad Kore Package Notation");
        }

        const kpn = `${repo}:${pkg}`;
        const match = pkgJson.dep.find((k) => k.startsWith(`${kpn}@`));
        if (!match) return term.printError(`${kpn} is not included`);

        pkgJson.dep.splice(pkgJson.dep.indexOf(match), 1);
        await FS.writestr(pkgPath, formatJSON(JSON.stringify(pkgJson)));

        const depPath = resolvePath(cwd, `kore_pkg/${repo}/${pkg}`);
        if (await FS.exists(depPath)) await FS.rmdir(depPath);
        if ((await FS.readdir(getParentPath(depPath))).length <= 0)
          await FS.rmdir(getParentPath(depPath));

        term.println(`${kpn} was removed`);
      }
    } else if (method === "list") {
      const path = resolvePath(cwd, "pkg.json");
      const pkgJson = JSON.parse(await FS.readstr(path));

      await term.println(
        `Packages in "${cwd}":\n${pkgJson.dep.toSorted().join("\n")}`
      );
    } else {
      term.printError("Bad method");
    }
  }

  showHelp(showTitle = 1) {
    if (showTitle) {
      term.println(term.color.blue("Kore JavaScript Runtime"));
    }
    term.println(
      ["Usage: kore <command> [arguments]", "", "Commands:"].join("\n")
    );
    for (const cmd of this.cmdReg) {
      term.println(
        `    ${cmd.name.join("/")}${cmd.args ? ` ${cmd.args.join(" ")}` : ""}${
          cmd.desc ? ` - ${cmd.desc}` : ""
        }`
      );
    }
    term.println("");
  }

  ontermination() {
    super.ontermination();

    this.resolve();
    if (this.rt) this.rt.kill();
  }
}
