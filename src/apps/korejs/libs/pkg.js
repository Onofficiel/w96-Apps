class KPN {
  constructor(kpnString) {
    const regex =
      /^(?:(\w+):)?([a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*)?(?:@(\d+\.\d+\.\d+(?:-[a-zA-Z0-9]+(?:\.\d+)?)?(?:\+[a-zA-Z0-9]+)?))?$/;
    const match = kpnString.match(regex);

    if (!match) {
      throw new Error("Invalid KPN string");
    }

    const [, repo, pkg, ver] = match;

    this.repo = repo || null;
    this.pkg = pkg || null;
    this.ver = ver ? new SemVer(ver) : null;
  }

  [Symbol.toPrimitive](hint) {
    if (hint === "string") {
      return this.toString();
    }
    return null;
  }

  toString() {
    let kpnString = "";
    if (this.repo) {
      kpnString += `${this.repo}:`;
    }
    if (this.pkg) {
      kpnString += `${this.pkg}`;
    }
    if (this.ver) {
      kpnString += `@${this.ver}`;
    }
    return kpnString;
  }

  static isValid(kpnString = "") {
    const regex =
      /^(?:(\w+):)?([a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*)?(?:@(\d+\.\d+\.\d+(?:-[a-zA-Z0-9]+(?:\.\d+)?)?(?:\+[a-zA-Z0-9]+)?))?$/;
    return regex.test(kpnString) && kpnString !== "";
  }
}

async function getPackages(path, repo) {
  const pkgPath = resolvePath(path, "pkg.json");
  if (!(await FS.isFile(pkgPath))) throw `"${pkgPath}" is not a file`;

  const pkgJson = JSON.parse(await FS.readstr(pkgPath));

  if (!pkgJson.rpo[repo])
    return term.printError(`Repository ${repo} does not exists`);

  const repoURL = new URL("Packages.json", pkgJson.rpo[repo]);
  const repoJson = await fetch(repoURL)
    .then((r) => r.json())
    .catch((e) => term.printError(`Error while fetch ${repo}: ${e}`));

  return repoJson;
}

async function installDeps(path) {
  const pkgPath = resolvePath(path, "pkg.json");
  if (!(await FS.isFile(pkgPath))) throw `"${pkgPath}" is not a file`;

  const pkgJson = JSON.parse(await FS.readstr(pkgPath));

  for (const dep of pkgJson.dep) {
    let { repo, pkg, ver } = new KPN(dep);
    if (!pkg || !repo) {
      return term.printError(
        "Incorrect Kore Package Notation (repo:pkg[@ver])"
      );
    }

    const repoPath = resolvePath(path, `kore_pkg/${repo}/${pkg}`);

    if (await FS.isFile(resolvePath(repoPath, `pkg.json`))) continue;

    if (!pkgJson.rpo[repo])
      // Repo check
      return term.printError(`Repository ${repo} does not exists`);

    const repoURL = new URL("Packages.json", pkgJson.rpo[repo]);
    const repoJson = await fetch(repoURL)
      .then((r) => r.json())
      .catch((e) => term.printError(`Error while fetch ${repo}: ${e}`));

    const pkgObj = repoJson.find((p) => p.name === pkg);

    // Changing version if needed
    ver ||= pkgObj.version;

    // Repo cloning
    if (!(await FS.exists(repoPath))) await FS.mkdir(repoPath);

    const zip = await fetch(
      new URL(
        resolvePath(pkgObj.rootPath, `content@${ver}.zip`),
        pkgJson.rpo[repo]
      ).toString()
    ).then((r) => (r.ok ? r.blob() : null));

    if (!zip)
      return term.printError(`The package cannot be found in version ${ver}`);

    const jszip = new JSZip();
    await jszip.loadAsync(zip);

    const pb = new ProgressBar(30, `${repo}:${pkg}`, term);

    let i = 0;
    for (const [path, data] of Object.entries(jszip.files)) {
      const folders = path.lastIndexOf("/");
      if (folders !== -1) {
        await FS.mkdir(resolvePath(repoPath, path.slice(0, folders)));
      }
      await FS.writebin(resolvePath(repoPath, path), await data.async("array"));

      pb.setProgress((++i / jszip.files.length) * 100, `Downloading files...`);
    }

    await installDeps(repoPath);

    pb.setProgress(100, ` Package ${pkg} from ${repo} added`);
    pb.stop();
  }
}

async function getAllDeps(path) {
  const deps = new Map();

  async function r(path) {
    const kpns = await getDeps(path);
    deps.set(path, kpns);

    for (const kpn of kpns) {
      const p = resolvePath(path, `kore_pkg/${kpn.replace(":", "/")}`);
      if (await FS.exists(p)) await r(p);
    }
  }

  await r(path);

  return deps;
}

async function getDeps(path) {
  const pkgJson = JSON.parse(await FS.readstr(resolvePath(path, "pkg.json")));

  return [
    ...new Set(
      pkgJson.dep.map((d) => {
        const { repo, pkg } = new KPN(d);
        return `${repo}:${pkg}`;
      })
    ),
  ];
}
