class SemVer {
  constructor(versionString = "0.0.0") {
    const regex =
      /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/;
    const match = versionString.match(regex);

    if (!match) {
      throw new Error("Invalid version string");
    }

    this.major = parseInt(match[1], 10);
    this.minor = parseInt(match[2], 10);
    this.patch = parseInt(match[3], 10);
    this.preRelease = match[4] ? match[4].split(".") : [];
    this.build = match[5] || "";
  }

  [Symbol.toPrimitive](hint) {
    if (hint === "string") {
      return (
        `${this.major}.${this.minor}.${this.patch}` +
        (this.preRelease.length ? `-${this.preRelease.join(".")}` : "") +
        (this.build ? `+${this.build}` : "")
      );
    } else if (hint === "number") {
      return this.major * 1000000 + this.minor * 1000 + this.patch;
    } else {
      return null;
    }
  }

  static compare(versionA, versionB) {
    if (!(versionA instanceof this) || !(versionB instanceof this)) {
      throw new Error("Invalid version objects");
    }

    const valueA = Number(versionA);
    const valueB = Number(versionB);

    if (valueA < valueB) {
      return -1;
    } else if (valueA > valueB) {
      return 1;
    } else {
      return 0;
    }
  }

  static isLowerThan(versionA, versionB) {
    return this.compare(versionA, versionB) === -1;
  }

  static isGreaterThan(versionA, versionB) {
    return this.compare(versionA, versionB) === 1;
  }

  static isEqual(versionA, versionB) {
    return this.compare(versionA, versionB) === 0;
  }

  isLowerThan(version) {
    return this.constructor.compare(this, version) === -1;
  }

  isGreaterThan(version) {
    return this.constructor.compare(this, version) === 1;
  }

  isEqual(version) {
    return this.constructor.compare(this, version) === 0;
  }

  toString() {
    return String(this);
  }
}
