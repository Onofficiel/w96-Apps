/* * * * * * *\
* (c) Kelbaz  *
\* * * * * * */

let getTableStr;
let createTable;
{
  function objectToString(value) {
    return Object.prototype.toString.call(value);
  }

  function formatValue(value) {
    const fmt = inspect(value);

    if (fmt.includes("\n")) return String(value);
    return fmt;
  }

  function GetTableStr(data) {
    const rows = [];

    if (Array.isArray(data)) {
      if (data.length === 0) {
        rows.push([["(empty)"]]);
      } else if (
        data.map(Array.isArray).reduce((a, b) => a && b) &&
        data.map((v) => v.length).reduce((a, b) => (a === b ? a : false)) !==
          false
      ) {
        rows.push([["(idx)"].concat(Object.keys(data))]);

        const row = [];
        for (const i in data) {
          row.push([i].concat(data[i].map(formatValue)));
        }
        rows.push(row);
      } else {
        rows.push([["(idx)", "Values"]]);
        const row = [];
        for (const i in data) {
          row.push([i, formatValue(data[i])]);
        }
        rows.push(row);
      }
    } else if (objectToString(data) === "[object Map]") {
      rows.push([["(key)", "Values"]]);

      const row = [];
      for (const [key, value] of data) {
        row.push([formatValue(key), formatValue(value)]);
      }
      rows.push(row);
    } else if (objectToString(data) === "[object Set]") {
      rows.push([["(idx)", "Values"]]);

      let i = 0;
      const row = [];
      for (const value of data) {
        row.push([String(i++), formatValue(value)]);
      }
      rows.push(row);
    } else if (typeof data === "object" && data !== null) {
      rows.push([["(key)", "Values"]]);

      const row = [];
      for (const key in data) {
        const value = data[key];
        row.push([key, formatValue(value)]);
      }
      rows.push(row);
    } else {
      rows.push([["Invalid data format"]]);
    }

    return CreateTable(rows);
  }

  function CreateTable(blocks) {
    const tableLines = [];

    let gWidths = getGreaterWidths(blocks);

    tableLines.push(
      `┌${gWidths.map((width) => "─".repeat(width + 2)).join("┬")}┐`
    );

    blocks.forEach((block, i) => {
      block.forEach((row) => {
        tableLines.push(
          `│ ${row.map((r, i) => r.padEnd(gWidths[i])).join(" │ ")} │`
        );
      });
      if (i < blocks.length - 1) {
        tableLines.push(
          `├${gWidths.map((width) => "─".repeat(width + 2)).join("┼")}┤`
        );
      }
    });

    tableLines.push(
      `└${gWidths.map((width) => "─".repeat(width + 2)).join("┴")}┘`
    );

    return tableLines.join("\n");
  }

  function transpose(arr) {
    return Object.keys(arr[0]).map((column) => arr.map((row) => row[column]));
  }

  function getGreaterWidths(table) {
    const widths = [];
    let block = transpose(table.flat());

    block.forEach((column) => {
      const w = column.reduce((a, b) => {
        return a.length > b.length ? a : b;
      }).length;

      widths.push(w);
    });

    return widths;
  }

  getTableStr = GetTableStr;
  createTable = CreateTable;
}
