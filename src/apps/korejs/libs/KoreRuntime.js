/** @type {import("./Runtime.js")} */
class KoreRuntime extends Runtime {
  srcPage = "/_/C/local/korejs/env.html";
  importMap = new Map([
    ["$/", "/_/"],
    ["kore/test", "/_/C/user/documents/test_kore/mod.js"],
  ]);

  exited = false;

  constructor(term) {
    super();
    this.term = term;

    this.env.classList.add("korejs-env");
  }

  fmtPrint(data, color = true, { group = 0 } = {}) {
    let str = formatText(data)
      .map((v) =>
        typeof v === "string" ? v : inspect(v, { colors: color === true })
      )
      .join(" ");

    const padding = "  ".repeat(group);
    str = padding + str.replaceAll("\n", `\n${padding}`);

    if (typeof color === "string") {
      this.term.println(this.term.color[color](str));
    } else {
      this.term.println(str);
    }
  }

  async load(file, args, flags) {
    this.resolve;
    const promise = new Promise((r) => (this.resolve = r));

    // Packages importmap
    if (await FS.isFile(resolvePath(getParentPath(file), "pkg.json"))) {
      const deps = await getAllDeps(getParentPath(file));

      for (const [path, kpns] of deps) {
        const newPath = `/_/${path.replace(":", "")}/`;
        const scope = {};

        for (const kpn of kpns) {
          const depPath = `kore_pkg/${kpn.replace(":", "/")}`;

          const pkgJson = JSON.parse(
            await FS.readstr(resolvePath(path, `${depPath}/pkg.json`))
          );

          scope[kpn] = `${resolvePath(
            resolvePath(newPath, depPath),
            pkgJson.fle
          )}`;
        }

        this.scopeMap.set(newPath, scope);
      }
      console.log(Object.fromEntries(this.scopeMap));
    }

    const ctx = this;
    const Kore = new (class Kore {
      version = VERSION;
      path = `/${fixPath(file.replace(":", ""))}`;
      cwd = cwd;

      args = args;
      flags = flags;
      perms = [];

      fs = {};
      p3 = {};
      ui = {};
      os = {};

      util = {
        wait: (ms = 0) =>
          new Promise((r) => ctx.env.contentWindow.setTimeout(r, ms)),
        inspect,
        format: formatText,
        getTableStr,
      };
      processes = null;

      env = new (class Env {
        #map = new Map(
          w96.sys.env.envKeys().map((k) => [k, w96.sys.env.getEnv(k)])
        );

        constructor() {
          (async () => {
            let envPath = getParentPath(file);
            envPath += envPath.endsWith("/") ? "" : "/";
            envPath += ".env";

            if ((await FS.exists(envPath)) && (await FS.isFile(envPath))) {
              let envs = INI.parse(`[env]\n${await FS.readstr(envPath)}`).env;
              Object.entries(envs).forEach(([k, v]) => {
                this.#map.set(k, v);
              });
            }
          })();
        }

        get(key) {
          return this.#map.get(key);
        }

        has(key) {
          return this.#map.has(key);
        }

        set(key, value) {
          w96.sys.env.setEnv(key, value);
          this.#map.set(key, value);
        }

        toObject() {
          return Object.fromEntries(this.#map.entries());
        }
      })();

      exit() {
        ctx.kill();
      }

      panic(err) {
        ctx.fmtPrint([err], "red");
        ctx.kill();
      }
    })();

    Object.assign(Kore.os, {
      version: w96.sys.rel.getVersion(),
      version_str: w96.sys.rel.getVersionString(),
      buildId: await w96.sys.rel.getBuildId(),
    });

    if (
      flags.includes("--allow-read-fs") ||
      flags.includes("--allow-fs") ||
      flags.includes("--allow-all")
    ) {
      Kore.perms.push("fs.read");

      Object.assign(Kore.fs, {
        readstr: (path, ...args) =>
          FS.readstr(FSUtil.resolvePath(getParentPath(file), path), ...args),
        readStrChunk: (path, ...args) =>
          FS.readStrChunk(
            FSUtil.resolvePath(getParentPath(file), path),
            ...args
          ),

        readbin: (path, ...args) =>
          FS.readbin(FSUtil.resolvePath(getParentPath(file), path), ...args),
        readBinChunk: (path, ...args) =>
          FS.readBinChunk(
            FSUtil.resolvePath(getParentPath(file), path),
            ...args
          ),

        readdir: (path, ...args) =>
          FS.readdir(FSUtil.resolvePath(getParentPath(file), path), ...args),
        walk: (path, ...args) =>
          FS.walk(FSUtil.resolvePath(getParentPath(file), path), ...args),

        toURL: (path, ...args) =>
          FS.toURL(FSUtil.resolvePath(getParentPath(file), path), ...args),
        toBlob: (path, ...args) =>
          FS.toBlob(FSUtil.resolvePath(getParentPath(file), path), ...args),

        exists: (path, ...args) =>
          FS.exists(FSUtil.resolvePath(getParentPath(file), path), ...args),
        isFile: (path, ...args) =>
          FS.isFile(FSUtil.resolvePath(getParentPath(file), path), ...args),

        stat: (path, ...args) =>
          FS.stat(FSUtil.resolvePath(getParentPath(file), path), ...args),
        filetype: (path, ...args) =>
          FS.filetype(FSUtil.resolvePath(getParentPath(file), path), ...args),
      });
    }

    if (
      flags.includes("--allow-write-fs") ||
      flags.includes("--allow-fs") ||
      flags.includes("--allow-all")
    ) {
      Kore.perms.push("fs.write");

      Object.assign(Kore.fs, {
        writestr: (path, ...args) =>
          FS.writestr(FSUtil.resolvePath(getParentPath(file), path), ...args),
        writebin: (path, ...args) =>
          FS.writebin(FSUtil.resolvePath(getParentPath(file), path), ...args),

        touch: (path, ...args) =>
          FS.touch(FSUtil.resolvePath(getParentPath(file), path), ...args),
        mkdir: (path, ...args) =>
          FS.mkdir(FSUtil.resolvePath(getParentPath(file), path), ...args),

        rm: (path, ...args) =>
          FS.rm(FSUtil.resolvePath(getParentPath(file), path), ...args),
        rmdir: (path, ...args) =>
          FS.rmdir(FSUtil.resolvePath(getParentPath(file), path), ...args),

        mvfile: (path, path2) =>
          FS.mvfile(
            FSUtil.resolvePath(getParentPath(file), path),
            FSUtil.resolvePath(getParentPath(file), path2)
          ),
        mvdir: (path, path2) =>
          FS.mvdir(
            FSUtil.resolvePath(getParentPath(file), path),
            FSUtil.resolvePath(getParentPath(file), path2)
          ),

        cpfile: (path, path2) =>
          FS.cpfile(
            FSUtil.resolvePath(getParentPath(file), path),
            FSUtil.resolvePath(getParentPath(file), path2)
          ),
        cpdir: (path, path2) =>
          FS.cpdir(
            FSUtil.resolvePath(getParentPath(file), path),
            FSUtil.resolvePath(getParentPath(file), path2)
          ),

        rename: (path, ...args) =>
          FS.rename(FSUtil.resolvePath(getParentPath(file), path), ...args),
      });
    }

    this.keys.set("XMLHttpRequest", {
      value: class XMLHttpRequest {
        constructor() {
          throw new Error(
            "XMLHttpRequest was removed for security reasons. Please use fetch instead."
          );
        }
      },
    });
    if (flags.includes("--allow-net") || flags.includes("--allow-all")) {
      Kore.perms.push("net");

      const { p3 } = w96.net;
      Kore.p3 = {
        address: p3.address,
        getSecret() {
          return p3.secret;
        },

        connecting: p3.connecting,
        connected: p3.connected,

        error: p3.error,

        ports: p3.ports,
        connections: p3.connections,

        server: p3.server,

        createServer(ports, handler) {
          return p3.createServer(ports, handler);
        },
        createConnection(address) {
          return p3.createConnection(address);
        },

        connect() {
          return p3.connect();
        },
        disconnect() {
          return p3.disconnect();
        },

        discover(flags, limit) {
          return p3.discover(flags, limit);
        },
        publish(port, flags) {
          return p3.publish(port, flags);
        },
        unpublish(port) {
          return p3.unpublish(port);
        },

        getRandomPort() {
          return p3.getRandomPort();
        },
      };

      this.keys.set("fetch", {
        value: async function fetch(url, options) {
          const res = await window.fetch(url, options);
          const resUrl = new URL(res.url);

          if (
            !flags.includes("--allow-read-fs") &&
            !flags.includes("--allow-fs") &&
            !flags.includes("--allow-all") &&
            resUrl.origin === location.origin &&
            resUrl.pathname.startsWith("/_/")
          )
            throw new Error("Cannot fetch to SWFS without FS read access");

          return res;
        },
      });
    } else {
      function disabled() {
        throw new Error("Net access is not allowed");
      }

      this.keys.set("fetch", { value: disabled });
      this.keys.set("WebSocket", { value: disabled });
      this.keys.set("WebTransport", { value: disabled });
    }

    if (flags.includes("--allow-processes") || flags.includes("--allow-all")) {
      Kore.perms.push("processes");

      Kore.processes = {
        get: () => w96.state.processes.filter((p) => p !== null),
      };
    }

    if (flags.includes("--allow-ui") || flags.includes("--allow-all")) {
      Kore.perms.push("ui");

      Object.assign(Kore.ui, {
        WApplication: w96.WApplication,
        Theme: w96.ui.Theme,
        components: {
          MenuBar: w96.ui.MenuBar,
          DropZone: w96.ui.DropZone,
          ContextMenu: w96.ui.ContextMenu,
          ...w96.ui.components,
        },
        DialogCreator: w96.ui.DialogCreator,
        OpenFileDialog: w96.ui.OpenFileDialog,
        OperationDialog: w96.ui.OperationDialog,
        animateElement: w96.ui.animateElement,
      });
    }

    if (flags.includes("--allow-os") || flags.includes("--allow-all")) {
      Kore.perms.push("os");

      Object.defineProperties(Kore.os, {
        reboot: () => w96.sys.reboot(),
        execCmd: (...args) => w96.sys.execCmd(...args),
        execFile: (...args) => w96.sys.execFile(...args),
        renderBSOD: (message) => w96.sys.renderBSOD(message),
      });
    }

    function makePropertiesImmutable(obj, execptions = []) {
      Object.entries(obj).forEach(([key, value]) => {
        if (execptions.includes(value)) return;

        Object.defineProperty(obj, key, {
          writable: false,
          configurable: false,
        });

        if (typeof value === "object" && value !== null) {
          makePropertiesImmutable(value);
          Object.freeze(value);
        }
      });
    }
    makePropertiesImmutable(Kore, [Kore.processes]);
    Object.freeze(Kore);

    this.keys.set("Kore", {
      value: Kore,
      writable: false,
      configurable: false,
    });

    this.redefineConsole();
    this.redefineDialogs();

    super.load();
    await promise;
  }

  redefineConsole() {
    const ctx = this;

    const counts = new Map();
    const timers = new Map();

    let group = 0;

    this.keys.set("console", {
      value: new (class console {
        log(...data) {
          ctx.fmtPrint(data, true, { group });
        }
        table(data) {
          ctx.fmtPrint([getTableStr(data)], false, { group });
        }
        warn(...data) {
          ctx.fmtPrint(data, "yellow", { group });
        }
        info(...data) {
          ctx.fmtPrint(data, "blue", { group });
        }
        error(...data) {
          ctx.fmtPrint(data, "red", { group });
        }
        dir(...data) {
          ctx.fmtPrint(data, true, { group });
        }
        dirxml(...data) {
          ctx.fmtPrint(data, true, { group });
        }
        assert(condition, ...data) {
          if (condition) return;
          ctx.fmtPrint(
            ["Assertion failed" + (data.length ? ":" : ""), ...data],
            "red",
            { group }
          );
        }
        debug(...data) {
          ctx.fmtPrint(["Debug:", ...data], "blue", { group });
        }
        count(tag = "default") {
          if (!counts.has(tag)) counts.set(tag, 0);

          const count = counts.get(tag) + 1;
          counts.set(tag, count);
          ctx.fmtPrint([`${tag}: ${count}`], false, { group });
        }
        countReset(tag = "default") {
          if (!counts.has(tag)) {
            ctx.fmtPrint(`Count for '${tag}' does not exists`, "yellow", {
              group,
            });
            return;
          }

          counts.delete(tag, 0);
        }
        group(...data) {
          ctx.fmtPrint(data, true, { group });
          group++;
        }

        groupCollapsed(...data) {
          ctx.fmtPrint(data, true, { group });
          group++;
        }

        groupEnd() {
          group > 0 && group--;
        }

        clear() {
          ctx.term.clear();
        }

        pause() {
          ctx.term.pause();
        }

        trace(...data) {
          class Trace extends Error {}

          let stack = new Trace().stack;
          stack = stack.split("\n").slice(2).join("\n");

          ctx.fmtPrint(data, true, { group });
          ctx.fmtPrint([stack], false, { group });
        }

        time(tag = "default") {
          if (timers.has(tag))
            ctx.fmtPrint([`Timer '${tag}' already exists`], "yellow", {
              group,
            });
          else timers.set(tag, Date.now());
        }

        timeLog(tag = "default") {
          if (!timers.has(tag)) {
            ctx.fmtPrint([`Timer '${tag}' does not exists`], "yellow", {
              group,
            });
          } else {
            ctx.fmtPrint([`${tag}: ${Date.now() - timers.get(tag)}ms`], false, {
              group,
            });
          }
        }

        timeEnd(tag = "default") {
          if (!timers.has(tag)) {
            ctx.fmtPrint([`Timer '${tag}' does not exists`], "yellow", {
              group,
            });
          } else {
            ctx.fmtPrint([`${tag}: ${Date.now() - timers.get(tag)}ms`], false, {
              group,
            });
            timers.delete(tag);
          }
        }
      })(),
    });
  }

  redefineDialogs() {
    const { term } = this;

    this.keys.set("alert", {
      value: function alert(data) {
        term.println(data);
      },
    });

    this.keys.set("prompt", {
      value: function prompt(data) {
        return term.prompt(data);
      },
    });

    this.keys.set("confirm", {
      value: async function confirm(data) {
        let r = await term.prompt(`${data} (Yes/No): `, 1);

        if (r === null) {
          term.println("");
          return false;
        }
        if (["y", "yes"].includes(r.toLowerCase())) {
          return true;
        } else {
          return false;
        }
      },
    });
  }

  kill() {
    this.scopeMap = new Map();

    this.resolve();
    super.kill();
  }
}
