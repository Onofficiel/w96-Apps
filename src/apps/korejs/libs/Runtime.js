class Runtime {
  env;
  importMap = new Map();
  scopeMap = new Map();

  /** @type {Map<string, PropertyDescriptor>} */
  keys = new Map();
  srcPage = "/";

  constructor() {
    // create Environment
    this.env = document.createElement("iframe");

    Object.assign(this.env, {
      hidden: true,
      width: 0,
      height: 0,
    });
  }

  load() {
    // Set loading event
    this.env.addEventListener("load", () => {
      this.env.ctx = this.env.contentWindow;
      /** @type {Window} */
      const ctx = this.env.ctx;

      // Removing parent access
      delete ctx.parent;

      // Adding keys to the env

      this.keys.forEach((v, k) => {
        if (!v.value) return;

        ctx[k] = v.value;
        Object.defineProperty(ctx, k, v);
      });

      // Load ImportMap
      const im = ctx.document.createElement("script");
      im.type = "importmap";
      im.innerText = JSON.stringify({
        imports: {
          ...Object.fromEntries(this.importMap),
        },
        scopes: {
          ...Object.fromEntries(this.scopeMap),
        }
      });

      // Check for unloads
      ctx.addEventListener("beforeunload", () => {
        this.kill();
      });

      // Add the iframe to the DOM
      ctx.document.body.insertAdjacentElement("afterbegin", im);

      // Mutation Observer
      function handleMutation(mutationsList, observer) {
        observer.disconnect();

        mutationsList.forEach((mutation) => {
          mutation.addedNodes.forEach((node) => {
            node.remove();
          });

          mutation.attributeName &&
            mutation.target.removeAttribute(mutation.attributeName);

          mutation.removedNodes.forEach((node) => {
            mutation.target.insertBefore(node, mutation.nextSibling);
          });
        });
      }

      const observerOptions = {
        childList: true,
        attributes: true,
        subtree: true,
      };

      const observer = new MutationObserver(handleMutation);
      observer.observe(ctx.document.documentElement, observerOptions);

      ctx.postMessage("ready");
    });

    // Load Environment
    this.env.src = this.srcPage;
    document.body.appendChild(this.env);
  }

  kill() {
    this.env.src = "about:blank";
    this.env.remove();
  }
}
