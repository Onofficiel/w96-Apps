// Imports
let { term, cwd } = env;
const { resolvePath, fixPath, getParentPath, fname } = FSUtil;
const { ProgressBar } = w96.util.cli;
const { INI } = w96.util;

const VERSION = 1.0;

const ENV_HTML = `
<!DOCTYPE html>
<html>
  <body>
    <script defer>
      (async () => {
        let resolve;
        let promise = new Promise((r) => (resolve = r));

        window.addEventListener("message", ({ data }) => {
          if (data === "ready") resolve();
        });

        await promise;

        await import(\`$\${Kore.path}\`).catch(err => Kore.panic(err));
        Kore.exit();
      })();
    </script>
  </body>
</html>
`.trim();

const PKG_FILE_COLORS = [
  "blue",
  "brown",
  "red",
  "green",
  "orange",
  "pink",
  "purple",
  "yellow",
];

PKG_FILE_NAMES = [
  "David",
  "Gerald",
  "Theo",
  "Alice",
  "Benjamin",
  "Catherine",
  "World",
  "Daniel",
  "Emily",
  "Fiona",
  "George",
  "Hannah",
  "Isaac",
  "Julia",
  "Kenneth",
  "Kelbaz",
  "Lily",
  "Matthew",
  "Natalie",
  "Oliver",
  "Penelope",
  "Quincy",
  "Rachel",
  "Samuel",
  "VaultDweller",
  "Tiffany",
  "Ursula",
  "Banana",
  "Victor",
  "Wendy",
  "Xavier",
  "Yvonne",
  "Zara",
  "Abigail",
  "Benedict",
  "Charlotte",
  "Dominic",
  "Eleanor",
  "Hello",
  "Franklin",
  "Gabrielle",
  "Henry",
  "Isabella",
  "Jacob",
  "Katherine",
  "Liam",
  "Madeline",
  "Nathan",
  "Olivia",
  "Khaleesi",
  "Patrick",
  "Quinn",
  "Rebecca",
  "it's me",
  "Sebastian",
  "Tabitha",
  "Ulysses",
  "Victoria",
  "William",
  "Xena",
  "Yolanda",
  "Secret Agent",
  "Zachary",
  "Amelia",
  "Bartholomew",
  "Chloe",
  "Dylan",
  "Eva",
  "Felix",
  "Grace",
  "Harrison",
  "Hazel",
  "Ian",
  "Jasmine",
  "Kai",
  "Luna",
  "Milo",
  "Nora",
  "Oscar",
  "Wanderer",
  "Phoebe",
  "Quentin",
  "Rose",
  "Simon",
  "Sophia",
  "Thomas",
  "Violet",
  "Agent 42",
  "Neo",
  "Wyatt",
  "Inquisitor",
  "Spectre",
  "Jedi",
  "Geralt",
  "Xander",
  "Yara",
  "Zoe",
  "Arthur",
  "Beatrice",
  "Caleb",
  "Daisy",
  "Dragonborn",
  "Elijah",
  "Fiona",
  "Gideon",
  "Harper",
  "Isaiah",
  "Jade",
  "Kevin",
  "Lila",
  "Maxwell",
  "Naomi",
  "Owen",
  "Piper",
  "Quincy",
  "Riley",
  "Samson",
  "Tessa",
  "Uriah",
  "Valerie",
  "Wesley",
  "Ximena",
  "Yasmine",
  "Zion",
];
