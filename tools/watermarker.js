/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const fs = require('fs');
const pathMod = require('path');
const { die_error } = require('./lib/util/common');
const fsUtil = require('./lib/util/fs');
const config = require('../config/watermarker.json');
const confmt = require('./lib/util/confmt');

process.title = "ul-watermarker";

/**
 * Program entry point.
 */
async function main() {
    // Check if copyright hdr exists
    if(!await fsUtil.existsAsync("config/copyright_hdr.txt"))
        die_error(`Copyright header file "config/copyright_hdr.txt" is not present - Stop.`);

    
    /** @type {string} */
    let copyrightHdr;

    try {
        copyrightHdr = fs.readFileSync("config/copyright_hdr.txt", { encoding: 'utf-8' });
    } catch(e) {
        die_error(`Failed to read copyright header - ${e}`);
    }

    // Collect files to watermark
    console.log(confmt.format(`$[cyan]Collecting files to watermark...$[reset]`))

    // Process
    /** @type {string[]} */
    let files = (await fsUtil.walkAsync("./", (p)=>!p.includes("node_modules")))
        .filter(v => {
            // Check if root is valid
            let validRoot = false;

            for(let root of config.paths) {
                const formattedRoot = root.replace("/", pathMod.sep);

                if(v.startsWith(formattedRoot))
                    validRoot = true;
            }

            if(!validRoot)
                return false;
            
            // Check if extension is correct
            let hasExt = false;

            for(let ext of config.extensions)
                if(v.endsWith(ext))
                    hasExt = true;

            if(!hasExt)
                return false;

            return true;
        });
    
    // Read each file and process
    for(let file of files) {
        console.log(confmt.format(`$[cyan]Processing $[magenta]${file}$[reset]`))
        
        // Step 1. Create target watermark. Needed for comparison as well.
        let expectedWatermark;

        const fileExt = file.substring(file.lastIndexOf('.'));
        
        // Construct appropriate comment struct
        switch(fileExt) {
            default:
                throw new Error("Filetype not supported.");
            case ".css":
            case ".js":
                expectedWatermark = `/*\n${copyrightHdr}\n*/\n\n`
                break;
        }

        // Step 2. Extract existing watermark from file
        let fileContents = await fs.promises.readFile(file, { encoding: 'utf-8' });
        const watermark = fileContents.trim().substring(0, expectedWatermark.length);
        
        // Compare watermark
        if(watermark !== expectedWatermark) {
            console.log(confmt.format(`$[yellow]No watermark found - correcting...$[reset]`));
            fs.writeFileSync(file, `${expectedWatermark}${fileContents}`, { encoding: 'utf-8' });
            console.log(confmt.format(`$[cyan]Correction complete.$[reset]`));
        } else {
            console.log(confmt.format(`$[cyan]File is OK - watermark present.$[reset]`))
        }
    }
}

main();