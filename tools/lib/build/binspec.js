/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const BINSPEC_DEFAULT = {
    wrt: true,
    stripBsp: false, // Strip BinSpec?
    shiftArgv: false, // Whether to shift argv to remove the bin name
    /** @type {String} */
    name: null,
    friendlyName: "<Unknown>",
    /** @type {String} */
    icon: null,
    /** @type {String} */
    startupClass: null,
    description: "",
    author: "",
    copyright: "",
    version: 1.0,
    subsys: "gui", // gui or cli - just requires terminal to be opened.
    build: {
        /** @type {String[]} */
        jsIn: [], // JS Docs

        /** @type {String[]} */
        styleIn: [], // Style docs

        /** @type {String[]} */
        htmlIn: [] // Html docs
    }
}

module.exports = {
    BINSPEC_DEFAULT
}
