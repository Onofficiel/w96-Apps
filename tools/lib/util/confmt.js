/*
Windows 96 Userland Source Code.
Copyright (C) 2023 Windows96.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * Console text formatting library.
 */

const CONSOLE_FORMAT = {
    RED: "\u001b[31m",
    BLACK: "\u001b[30m",
    GREEN: "\u001b[32m",
    YELLOW: "\u001b[33m",
    BLUE: "\u001b[34m",
    MAGENTA: "\u001b[35m",
    CYAN: "\u001b[36m",
    WHITE: "\u001b[37m",
    RESET: "\u001b[0m",
    BOLD: "\u001b[1m",
    ULINE: "\u001b[4m",
    REVERSE: "\u001b[7m"
}

/**
 * Formats a string containing formatting codes.
 * @param {String} str The string to format.
 * @returns The formatted ANSI string.
 */
function format(str) {
    let newString = `${str}`; // Copy the string
    
    for(let formKey of Object.keys(CONSOLE_FORMAT))
        newString = newString.replace(new RegExp("\\$\\[" + formKey + "\\]", "gi"), CONSOLE_FORMAT[formKey]);

    return newString;
}

module.exports = {
    formCodes: CONSOLE_FORMAT,
    format
}